import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useState } from "react/cjs/react.production.min";

const schema = yup.object().shape({
  name: yup.string().required("Name is mandatory*"),
  mobile: yup.string().required("Mobile Number is mandatory*"),
  email: yup
    .string()
    .email("Please Enter a valid email")
    .required("Email is mandatory*"),
  age: yup
    .number()
    .positive("Age must be a positive number")
    .transform((v, o) => (o === "" ? null : v))
    .nullable()
    .required("Age is mandatory*"),
  // age: yup.number().min(18, 'Age must be greater than 18').required('Age is mandatory'),
  // age: yup.string().required('Age is mandatory').min(18, 'Age must be greater than 18').max(110, 'Age must be less than 110'),
  password: yup
    .string()
    .required("Password is mandatory*")
    .min(5, "Password must be minmum 5 characters"),
  confirmpassword: yup
    .string()
    .required("Confirm Password is mandatory*")
    .min(5, "Confirm Password must be 5 characters"),
});

const SignUp = () => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const submitForm = (values) => {
    console.log(values);
    localStorage.setItem("userdata", JSON.stringify(values));
    if (values) {
      router.push ('/Login');
    }
  };

  return (
    <div className="bg-white w-[500px] shadow-2xl  m-auto mt-32">
      <div>
        <h1 className="text-center text-3xl font-semibold pt-5">
          Sign Up Form
        </h1>
      </div>
      <form
        className="flex flex-col items-center space-y-5"
        onSubmit={handleSubmit(submitForm)}
      >
        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Name</label>
          <input
            type="text"
            placeholder="Enter your name"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("name")}
          />
          <p className="text-red-600">{errors?.name?.message}</p>
        </div>
        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Mobile Number</label>
          <input
            type="text"
            placeholder="Enter your Mobile number"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("mobile")}
          />
          <p className="text-red-600">{errors?.mobile?.message}</p>
        </div>
        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Age</label>
          <input
            type="number"
            name="age"
            placeholder="Enter your Age"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("age")}
          />
          <p className="text-red-600">{errors?.age?.message}</p>
        </div>

        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Email</label>
          <input
            type="email"
            name="email"
            placeholder="Enter your email"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("email")}
          />
          <p className="text-red-600">{errors?.email?.message}</p>
        </div>
        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Password</label>
          <input
            type="password"
            name="password"
            placeholder="Enter your password"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("password")}
          />
          <p className="text-red-600">{errors?.password?.message}</p>
        </div>
        <div className="flex flex-col space-y-3 mt-4">
          <label className="text-lg">Confirm Password</label>
          <input
            type="password"
            name="confirmpassword"
            placeholder="Enter your confirm Password"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("confirmpassword")}
          />
          <p className="text-red-600">
            {errors?.message?.password
              ? errors?.confirmpassword?.message && "Password didnot match"
              : errors?.confirmpassword?.message}
          </p>
        </div>
        {/* {checkResult === false ?  */}
        <div>
          <input
            type="submit"
            value="Sign Up"
            className="bg-pink-600 p-3 mb-5 rounded-lg w-40 font-medium text-white cursor-pointer"
          />
        </div> 
        {/* : <Link href='/Login'>
        <div>
          <input
            type="submit"
            value="Sign Up"
            className="bg-pink-600 p-3 mb-5 rounded-lg w-40 font-medium text-white cursor-pointer"
          />
        </div>
        </Link> } */}
      </form>
    </div>
  );
};

export default SignUp;
