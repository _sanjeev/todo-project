import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useRouter } from "next/router";
import { yupResolver } from "@hookform/resolvers/yup";
import Loader from "../components/Loader";
import { useEffect, useState } from "react";

const loginData = yup.object().shape({
  mobile: yup.string().required("Mobile Number is mandatory*"),
  password: yup
    .string()
    .required("Password is mandatory*")
    .min(5, "Password must be minmum 5 characters"),
});

const Login = () => {
  const router = useRouter();
  const [isValid, setValid] = useState(false);
  //   console.log(await loginData.isValid());

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginData),
  });

  

  const login = (value) => {
    const result = JSON.parse(localStorage.getItem("userdata"));
    console.log(result);
    if (result.mobile !== value.mobile && result.password !== value.password) {
        alert ('Mobile Number and password doesnot match');
    }else if (result.mobile !== value.mobile) {
        alert ('Mobile Number doesnot match');
    }else if (result.password !== value.password) {
        alert ('Password doenot match');
    }else {
        alert('Successfully Login....')
        router.push ('/AllTodo');
    }
  };

  return (
    <div className="w-[500px] m-auto shadow-lg bg-white mt-[200px]">
      <div>
        <h1 className="text-center text-3xl pt-5 font-medium pb-5">
          Login Form
        </h1>
      </div>
      <form
        onSubmit={handleSubmit(login)}
        className="flex flex-col items-center space-y-5"
      >
        <div className="flex flex-col space-y-5">
          <label className="text-lg">Phone Number</label>
          <input
            type="text"
            placeholder="Enter your Phone Number"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("mobile")}
          />
          <p className="text-red-600">{errors?.mobile?.message}</p>
        </div>
        <div className="flex flex-col space-y-5">
          <label className="text-lg">Password</label>
          <input
            type="password"
            placeholder="Enter your Password"
            className="border-2 border-gray-500 w-80 text-xl hover:outline-none focus:outline-none"
            {...register("password")}
          />
          <p className="text-red-600">{errors?.password?.message}</p>
        </div>
        <div className="flex flex-col space-y-5 p-5">
          <input
            type="submit"
            value="Login"
            className="bg-pink-600 p-4 cursor-pointer rounded-lg w-56 font-semibold text-lg text-white"
          />
        </div>
      </form>
    </div>
  );
};

export default Login;
