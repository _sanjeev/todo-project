import {useRouter} from 'next/router';
import {useState} from 'react';

export const getStaticProps = async () => {
  const response = await fetch("http://localhost:3000/todos");
  const data = await response.json();
  return {
    props: {
      allTodo: data,
    },
  };
};
const AllTodo = ({ allTodo }) => {
console.log("🚀 ~ file: AllTodo.js ~ line 14 ~ AllTodo ~ allTodo", allTodo)

  const [todos, setTodos] = useState (allTodo);
  console.log(todos);

  const router = useRouter();

  const editTodo = async(id) => {
      router.push ('/Todo/' + id);
  }

  const deleteTodo = async(id) => {
    
    const out = await fetch('http://localhost:3000/todos/' + id, {
        method: "delete"
    });
    const result = todos.filter((key) => {
        return key.id !== id;
    })
    setTodos(result);
  }

  const addTodo = () => {
      router.push ('/Todo');
  }

  return (
    <div className="mx-auto">
      <div>
        <h1 className=" text-center text-3xl py-5">All Todos</h1>
      </div>
      <div className="flex justify-center">
        <button className="text-3xl bg-pink-600 p-2 rounded text-white" onClick={addTodo}>
          Add Todo
        </button>
      </div>
      <div className="flex flex-wrap justify-center mt-10">
        {todos.map((key) => (
          <div key={key.id} className="w-[350px] shadow-2xl m-3 bg-white space-y-3 p-4">
              <p className="text-lg">{key.name}</p>
              <p className="text-lg">{key.description}</p>
              <p className="text-lg">{key.status}</p>
              <button className="text-white bg-pink-600 p-1 mr-2 rounded" onClick={() => {
                  editTodo(key.id)
              }}>Edit Todo</button>
              <button className="text-white bg-pink-600 p-1 rounded" onClick={() => {
                  deleteTodo(key.id)
              }}>Delete Todo</button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AllTodo;
