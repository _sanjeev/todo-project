import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useRouter } from "next/router";
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup.object().shape({
  name: yup.string().required("Name is mandatory*"),
  description: yup.string().required("Description is Mandatory*"),
});

const Todo = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const router = useRouter();

  const addTodo = async(value) => {
    console.log(value);
    const res = await fetch("http://localhost:3000/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(value),
    });
    const data = await res.json();
    router.push ('/AllTodo');
  };

  return (
    <div className="w-[500px] m-auto shadow-xl bg-white mt-56">
      <div className="mt-10 py-5">
        <h1 className="font-medium text-3xl text-center">Add Todo</h1>
      </div>
      <form
        onSubmit={handleSubmit(addTodo)}
        className="flex flex-col items-center justify-center space-y-5"
      >
        <div className="flex flex-col space-y-5">
          <label className="text-lg">Name</label>
          <input
            type="text"
            placeholder="Enter Name of todo"
            className="border-2 border-gray-500 w-80 text-lg hover:outline-none focus:outline-none"
            {...register("name")}
          />
          <p className="text-red-600">{errors?.name?.message}</p>
        </div>
        <div className="flex flex-col space-y-5">
          <label className="text-lg">Description</label>
          <input
            type="text"
            placeholder="Add Description here"
            className="border-2 border-gray-500 w-80 text-lg hover:outline-none focus:outline-none"
            {...register("description")}
          />
          <p className="text-red-600">{errors?.description?.message}</p>
        </div>
        <div className="flex flex-col space-y-5">
          <label className="text-lg">Status</label>
          <select
            name="cars"
            id="cars"
            className="w-80 py-4 hover:outline-none focus:outline-none"
            {...register("status")}
          >
            <option value="inprogress">InProgress</option>
            <option value="completed">Completed</option>
          </select>
        </div>
        <div>
          <input
            type="submit"
            value="Add Todo"
            className="p-4 mt-10 bg-pink-600 my-5 rounded-lg w-48 font-semibold cursor-pointer text-white"
          />
        </div>
      </form>
    </div>
  );
};

export default Todo;
